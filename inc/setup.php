<?php

function alkitab_setup_theme() {
  register_nav_menu( 'primary', __( 'Primary Menu', 'alkitab' ) );
  register_nav_menu( 'profile', __( 'Profile Menu', 'alkitab' ) );
}

function alkitab_setup_widgets() {
  register_sidebar(
    array(
      'id' => 'alkitab_page_sidebar',
      'name' => __( 'Page widget area', 'alkitab' ),
      'description' => __( 'Default page sidebar', 'alkitab' ),
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h6>',
      'after_title' => '</h6>'
    )
  );

  register_sidebar(
    array(
      'id' => 'alkitab_footer_one',
      'name' => __( 'Footer First Column', 'alkitab' ),
      'before_widget' => '<div id="%1$s" class="c-footer--widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h6 class="c-footer--widget--title">',
      'after_title' => '</h6>'
    )
  );

  register_sidebar(
    array(
      'id' => 'alkitab_footer_two',
      'name' => __( 'Footer Second Column', 'alkitab' ),
      'before_widget' => '<div id="%1$s" class="c-footer--widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h6 class="c-footer--widget--title">',
      'after_title' => '</h6>'
    )
  );

  register_sidebar(
    array(
      'id' => 'alkitab_footer_three',
      'name' => __( 'Footer Third Column', 'alkitab' ),
      'before_widget' => '<div id="%1$s" class="c-footer--widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h6 class="c-footer--widget--title">',
      'after_title' => '</h6>'
    )
  );
}