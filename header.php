<?php
  $logotypes = array(
    'dark' => get_template_directory_uri() . '/assets/images/logo-dark.png',
    'light' => get_template_directory_uri() . '/assets/images/logo-light.png',
    'default' => get_template_directory_uri() . '/assets/images/logo.png'
  );
?>
<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php bloginfo( 'title' ); ?></title>
  <?php wp_head(); ?>
</head>
<body>
  <!-- ROOT -->
  <div id="root">

    <!-- NAVIGATION -->
    <nav id="navigation" class="l-nav-container">
      <!-- MOBILE -->
      <div class="l-mnav" data-mobile-navigation>
        <div class="c-mnav">
          <i class="c-mnav--menu" data-menu-toggle data-feather="menu"></i>
          <a class="c-mnav--homelink" href="<?= home_url(); ?>">
            <img
              class="c-mnav--logo c-logo--dark"
              src="<?php echo $logotypes['dark']; ?>"
              alt="alkitab logo"
            >
            <img
              class="c-mnav--logo c-logo--light"
              src="<?php echo $logotypes['light']; ?>"
              alt="alkitab logo"
            >
            <img
              class="c-mnav--logo c-logo"
              src="<?= $logotypes['default'] ?>"
              alt="alkitab logo"
            >
          </a>
        </div>
        <div class="c-offcanvas non-logged" data-offcanvas>
          <?php
            wp_nav_menu( array(
              'theme_location' => 'primary',
              'container' => false,
              'menu_class' => 'c-offcanvas--menu'
            ) );
          ?>
        </div>
        <div class="c-overlay" data-overlay></div>
      </div><!-- END MOBILE -->

      <!-- TABLET-UP -->
      <div class="c-nav">
        <a class="c-nav--homelink" href="<?= home_url(); ?>">
          <img
            class="c-nav--logo c-logo--dark"
            src="<?= $logotypes['dark'] ?>"
            alt="alkitab logo"
          >
          <img
            class="c-nav--logo c-logo--light"
            src="<?= $logotypes['light'] ?>"
            alt="alkitab logo"
          >
          <img
              class="c-nav--logo c-logo"
              src="<?= $logotypes['default'] ?>"
              alt="alkitab logo"
            >
        </a>
        <?php
          wp_nav_menu( array(
            'theme_location' => 'primary',
            'container' => false,
            'menu_class' => 'c-nav--menu'
          ) );
        ?>
      </div><!-- END TABLET-UP -->
    </nav><!-- END NAVIGATION -->