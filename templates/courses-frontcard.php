<div class="c-course-card bright">
  <?php
    if (has_post_thumbnail()) {
      the_post_thumbnail( 'full', array( 'class' => 'c-course-card--thumbnail' ) );
    }
  ?>
  <div class="c-course-card--details">
    <span class="c-course-card--title"><?php the_title(); ?></span>
    <a
      class="c-course-card--link"
      href="<?= the_permalink(); ?>"
    >
      <?= __( 'Go to course', 'alkitab' ); ?>
    </a>
  </div>
</div>