<?php
if ( is_user_logged_in() ) {
  get_header( 'logged' );
} else {
  get_header();
}
?>

<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

  <div class="l-page-container">
    <main class="l-page">

      <?php if ( has_post_thumbnail() ): ?>
        <div class="c-page--thumbnail">
          <?php
            the_post_thumbnail(
              'full',
              array(
                'class' => 'c-page-thumbnail'
              )
            );
          ?>
        </div>
      <?php endif; ?>

      <div class="c-page--details">
        <h1 class="c-page--title">
          <?php the_title(); ?>
        </h1>
        <span class="c-page--date">
          <?php the_date(); ?>
        </span>
      </div>
      <div class="c-page--content">
        <?php the_content(); ?>
      </div>
    </main>

    <aside class="l-page--sidebar">
      <?php get_sidebar(); ?>
    </aside>
  </div>
<?php endwhile; endif; ?>

<?php
get_footer();