<?php

// setup
add_theme_support( 'menu' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );


// includes
require_once get_template_directory() . '/inc/setup.php';
require_once get_template_directory() . '/inc/front/enqueue.php';
require_once get_template_directory() . '/inc/front/utils.php';
require_once get_template_directory() . '/inc/front/customize.php';
require_once get_template_directory() . '/inc/front/ajax.php';

// hooks
alkitab_setup_ajax();

add_action( 'wp_enqueue_scripts', 'alkitab_enqueue_scripts' );
add_action( 'after_setup_theme', 'alkitab_setup_theme' );
add_action( 'widgets_init', 'alkitab_setup_widgets' );
add_action( 'customize_register', 'alkitab_customize_register' );
add_action( 'template_redirect', 'alkitab_redirect_logged_users' );
add_action( 'wp_nav_menu_items', 'alkitab_add_logout_menu_item', 10, 2 );

// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );

// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );

add_filter( 'show_admin_bar', 'alkitab_show_admin_bar', 10, 1 );
function alkitab_show_admin_bar( $show ) {
  $user_meta = get_userdata( get_current_user_id() );
  $roles = [];
  if ( $user_meta ) {
    $roles = $user_meta->roles;
  }

  if ( count( $roles ) == 1 && in_array( 'student', $roles ) ) {
    return false;
  }

  return $show;
}

// shortcodes