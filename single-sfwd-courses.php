<?php
  if ( is_user_logged_in() ) {
    get_header( 'logged' );
  } else {
    get_header();
  }

  if ( have_posts() ): while ( have_posts() ): the_post();
?>
  <div class="c-course--header">
    <?php
      the_post_thumbnail( 'full', array( 'class' => 'c-course--image' ) );
    ?>
    <h1 class="c-course--title"><?php the_title(); ?></h1>
  </div>
  <div class="c-course--content">
    <?php the_content(); ?>
  </div>

<?php
    endwhile;
  endif;
get_footer();