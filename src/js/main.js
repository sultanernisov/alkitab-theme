import $ from 'jquery';
import feather from 'feather-icons';
import MobileNavigation from './components/MobileNavigation';
import TransitionComponent from './components/TransitionComponent';
import HeaderAutohide from  './components/HeaderAutohide';
import errorMessages from './components/ErrorMessage';
import TaxonomyFeed from './components/TaxonomyFeed';

$(document).ready(() => {
  feather.replace();
  
  const mNav = new MobileNavigation('[data-mobile-navigation]');
  mNav.init();
  
  const transition = new TransitionComponent('[data-transition-component]');

  transition.init(({ entry, sections }) => {
    const navigation = document.getElementById('navigation');
    sections.forEach(section => {
      if (navigation.classList.contains(section.dataset.section)) {
        navigation.classList.remove(section.dataset.section);
      }
    });
    navigation.classList.add(entry.target.dataset.section);
  });

  let autohide = new HeaderAutohide('#navigation');
  autohide.init();

  errorMessages();

  let taxonomyFeed = new TaxonomyFeed(document.querySelector('[data-taxonomy]'));
  taxonomyFeed.init();
});