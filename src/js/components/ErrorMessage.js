import $ from 'jquery';

export default () => {
  let messages = document.querySelectorAll('[data-error-message]');
  let inputs = document.querySelectorAll('[data-hide-errors]');

  messages.forEach(closeMessages);
  inputs.forEach(closeMessagesOnFocus);
};

const closeMessages = message => {
  let closeButton = message.querySelector('[data-error-close]');
  closeButton.addEventListener('click', () => {
    let focusedInput = document.querySelector('[data-after-error-focus]');
    
    const afterAnimation = () => $(focusedInput).focus();

    hideMessage(message, afterAnimation);
  });
};

const closeMessagesOnFocus = (input) => {
  input.addEventListener('focus', () => {
    const message = document.querySelector('[data-error-message]');
    hideMessage(message);
  });
};

const hideMessage = (message, callback) => {
  $(message).animate({
    height: "0px",
    padding: "0"
  }, 200, () => {
    $(message).remove();
    callback && callback();
  });
};