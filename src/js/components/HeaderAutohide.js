export default class HeaderAutohide {
  constructor(selector) {
    this.header = document.querySelector(selector);
    this.prevScroll = window.pageYOffset;
    this.root = document.getElementById('root');
  }

  init() {
    if (!this.header) return;
    window.addEventListener('scroll', this.onScroll.bind(this));
  }

  onScroll() {
    let current = window.pageYOffset;
    if (current > 60) {
      this.header.style = 'position: fixed; top: 0; box-shadow: 0 1px 15px 0 rgba(150, 150, 150, .2);';
    }

    if ((this.prevScroll - current) < 0 && current > 60) {
      this.header.classList.add('hidden');
      this.root.style.marginTop = '60px';
    }
    
    if ((this.prevScroll - current) > 0) {
      this.header.classList.remove('hidden');
      this.root.style.marginTop = '0px';
    }

    if (current === 0) {
      this.header.style = '';
    }
    this.prevScroll = current;
  }
}