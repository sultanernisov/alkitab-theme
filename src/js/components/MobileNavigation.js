export default class MobileNavigation {
  constructor(selector) {
    this.mNav = document.querySelector(selector);
    if (!this.mNav) return;

    this.toggle = this.mNav.querySelector('[data-menu-toggle]');
    this.offcanvas = this.mNav.querySelector('[data-offcanvas]');
    this.overlay = this.mNav.querySelector('[data-overlay]');
    this.searchToggle = this.mNav.querySelector('[data-search-toggle]');
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.openSearch = this.openSearch.bind(this);
    this.closeSearch = this.closeSearch.bind(this);
  }

  init() {
    if (!this.mNav) return;

    this.toggle.addEventListener('click', this.open);
    this.overlay.addEventListener('click', this.close);
    this.overlay.addEventListener('click', this.closeSearch);
    if (this.searchToggle) {
      this.searchToggle.addEventListener('click', this.openSearch);
    }
  }

  open() {
    this.mNav.classList.remove('search');
    this.mNav.classList.add('open');
  }

  close() {
    this.mNav.classList.remove('open');
  }

  openSearch() {
    this.mNav.classList.remove('open');
    this.mNav.classList.add('search');
  }

  closeSearch() {
    this.mNav.classList.remove('search');
  }
}