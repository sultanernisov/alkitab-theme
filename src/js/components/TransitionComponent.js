export default class TransitionComponent {
  constructor(selector) {
    this.container = document.querySelector(selector);
    if (!this.container) return;

    this.sections = this.container.querySelectorAll('[data-section]');
  }

  init(callback) {
    if (!this.container) return;
    this.usercb = callback;

    this.options = {
      threshold: .5
    };

    this.observer = new IntersectionObserver(this.callback.bind(this), this.options);
    this.sections.forEach(section => this.observer.observe(section));
  }

  callback(entries, observer) {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        this.container.className = 'c-transition-container ' + entry.target.dataset.section;
        this.usercb({
          entry: entry,
          container: this.container,
          sections: this.sections,
          observer: observer
        });
      }
    });
  }
}