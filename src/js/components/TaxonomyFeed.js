import $ from 'jquery';
import Spinner from './Spinner';
import LoadMore from './LoadMore';

export default class TaxonomyFeed {
  constructor(feed) {
    if (!feed) return;

    this.feed = feed;
    this.taxonomy = this.feed.dataset.taxonomy;
    this.term = this.feed.dataset.term;

    this.page = 1;
    this.loading = false;
    this.more = true;

    this.setLoading = this.setLoading.bind(this);
    this.showLoadMore = this.showLoadMore.bind(this);
  }

  init() {
    if (!this.feed) return;
    this.fetch();
  }

  fetch() {
    this.showLoadMore(false);
    this.setLoading(true);

    $.ajax({
      method: 'get',
      url: alkitab.ajaxurl,
      data: {
        action: 'alkitab_taxonomy_feed',
        term: this.term,
        taxonomy: this.taxonomy,
        page: this.page
      },
      success: (response) => {
        this.setLoading(false);
        if (response) {
          console.log(response);
          this.feed.innerHTML =this.feed.innerHTML + response.courses;
          this.page = this.page + 1;
          this.more = this.page <= response.pages;
          if (this.more) {
            this.showLoadMore(true);
          }
        }
      },
      error: (err) => {
        this.setLoading(false);
        console.log(err);
      }
    })
  }

  showLoadMore(show) {
    if (show) {
      let loadmore = LoadMore(() => {
        this.fetch();
      });
      this.feed.appendChild(loadmore);
    } else {
      let loadmore = this.feed.querySelector('[data-loadmore]');
      if (loadmore) loadmore.remove();
    }
  }

  setLoading(loading) {
    let spinner = Spinner();
    this.loading = loading;

    if (loading) {
      this.feed.appendChild(spinner);
    } else {
      let s = this.feed.querySelector('.la-ball-clip-rotate');
      if (s) {
        s.closest('.row-center').remove();
      }
    }
  }
}