
  </div>

  <footer class="l-footer">
    <div class="l-footer--col">
      <img
        class="c-footer--logo"
        src="<?= get_template_directory_uri() . '/assets/images/logo-light.png'; ?>"
        alt="alkitab logo"
      >
      <?php get_sidebar( 'footer-one' ); ?>
    </div>

    <div class="l-footer--col">
      <?php get_sidebar( 'footer-two' ); ?>
    </div>

    <div class="l-footer--col">
      <?php get_sidebar( 'footer-three' ); ?>
    </div>
  </footer>

  <?php wp_footer(); ?>
</body>
</html>