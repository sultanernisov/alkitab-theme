import { src, dest, watch, series, parallel } from 'gulp';
import yargs from 'yargs';
import sass from 'gulp-sass';
import cleanCss from 'gulp-clean-css';
import gulpif from 'gulp-if';
import postcss from 'gulp-postcss';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'autoprefixer';
import imagemin from 'gulp-imagemin';
import del from 'del';
import webpack from 'webpack-stream';
import named from 'vinyl-named';
import browserSync from 'browser-sync';
import zip from 'gulp-zip';
import info from './package.json';
import wpPot from 'gulp-wp-pot';
const PRODUCTION = yargs.argv.prod;

const server = browserSync.create();
export const serve = done => {
  server.init({
    proxy: 'alkitab.lan'
  });
  done();
};

export const serveMac = done => {
  server.init({
    proxy: 'localhost:8080/alkitab/'
  });
  done();
};

export const reload = done => {
  server.reload();
  done();
};

export const styles = () => {
  // let glob = ['src/scss/bundle.scss', 'src/scss/admin.scss'];
  return src('src/scss/*.scss')
    .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulpif(PRODUCTION, postcss([ autoprefixer ])))
    .pipe(gulpif(PRODUCTION, cleanCss({ compatibility: 'ie8' })))
    .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
    .pipe(dest('assets/css'))
    .pipe(server.stream());
};

export const images = () => {
  return src('src/images/**/*.{jpg,jpeg,png,svg,gif}')
    .pipe(gulpif(PRODUCTION, imagemin()))
    .pipe(dest('assets/images'))
    .pipe(server.stream());
};

export const scripts = () => {
  // let glob = ['src/js/bundle.js', 'src/js/admin.js', 'src/js/frontpage.js'];
  return src('src/js/*.js')
    .pipe(named())
    .pipe(webpack({
      module: {
        rules: [
          {
            test: /\.js$/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: []
              }
            }
          }
        ]
      },
      mode: PRODUCTION ? 'production' : 'development',
      devtool: !PRODUCTION ? 'inline-source-map' : false,
      output: {
        filename: '[name].js'
      },
      externals: {
        jquery: 'jQuery'
      }
    }))
    .pipe(dest('assets/js'))
    .pipe(server.stream());
};

export const clean = () => del(['assets, bundled']);

export const watchChanges = () => {
  watch('src/scss/**/*.scss', styles);
  watch('src/images/**/*.{jpg,jpeg,png,svg,gif}', images);
  watch('src/js/**/*.js', scripts);
  watch('**/*.php', reload);
};

export const compress = () => {
  return src([
    '**/*',
    '!node_modules{,/**}',
    '!bundled{,/**}',
    '!src{,/**}',
    '!.babelrc',
    '!.gitignore',
    '!gulpfile.babel.js',
    '!package.json',
    '!package-lock.json',
  ])
  .pipe(dest(`bundled/${info.name}/`))
  .pipe(zip(`${info.name}.zip`))
  .pipe(dest('bundled'));
};

export const pot = () => {
  return src('**/*.php')
    .pipe(
      wpPot({
        domain: 'alkitab',
        package: info.name
      })
    )
    .pipe(dest(`languages/${info.name}.pot`));
};

export const dev = series(clean, parallel(styles, images, scripts), serve, watchChanges);
export const build = series(clean, parallel(styles, images, scripts), pot, compress);
export const devMac = series(clean, parallel(styles, images, scripts), serveMac, watchChanges);
export default dev;
