<?php

if ( is_user_logged_in() ) {
  get_header( 'logged' );
} else {
  get_header();
}

if ( shortcode_exists( 'alkitab-course-grid' ) ) {
  echo do_shortcode('[alkitab-course-grid]');
}


get_footer();