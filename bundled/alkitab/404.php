<?php
  if ( is_user_logged_in() ) {
    get_header( 'logged' );
  } else {
    get_header();
  }
?>

  <div class="c-404">
    <div class="c-404--message">
      <div>
        <h1 class="c-404--title">404</h1>
        <p><?= __( "Oops, looks like you're lost :(", 'alkitab' ); ?></p>
        <a href="<?= home_url( '/' ); ?>" class="c-404--home"><i data-feather="chevron-left"></i><?= __( "Go to home", 'alkitab' ); ?></a>
      
      </div>
    </div>

    <div class="c-404--latest-courses">
      <h2 class="c-404--latest-courses--title"><?= __( 'see our latest courses', 'alkitab' ); ?></h2>
      <?php
        $args = array(
          'post_type' => 'sfwd-courses',
          'posts_per_page' => 5
        );
        $courses = new WP_Query( $args );

        if ( $courses->have_posts() ) {
          while ( $courses->have_posts() ) {
            $courses->the_post();

            get_template_part( 'templates/courses', 'card' );

          }
        }

        wp_reset_postdata();
      ?>
    </div>
  </div>

<?php get_footer(); ?>