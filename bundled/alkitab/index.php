<?php
  if ( is_user_logged_in() ) {
    get_header( 'logged' );
  } else {
    get_header();
  }
?>

  <main class="c-feed">
    <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
      <article class="c-post-card">
        <?php
          if ( has_post_thumbnail() ) {
            the_post_thumbnail( 'full', array( 'class' => 'c-post-card--thumbnail' ) );
          }
        ?>
        <h3 class="c-post-card--title"><a class="c-post-card--title--link" href="<?= the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <span class="c-post--date"><?php the_date(); ?></span>
        <div>
          <?php the_excerpt(); ?>
        </div>
        <a href="<?= the_permalink() ?>" class="c-post-card--readmore"><?= __( 'Read More', 'alkitab' ); ?></a>
      </article>
    <?php endwhile;endif; ?>
  </main>

<?php get_footer(); ?>