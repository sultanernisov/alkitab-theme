<?php

function remove_image_size_attributes( $html ) {
  return preg_replace( '/(width|height)="\d*"/', '', $html );
}

function alkitab_redirect_logged_users() {
  if ( !current_user_can( 'manage_options' ) && is_user_logged_in() && is_front_page() ) {
    $redirect_page = get_permalink( get_theme_mod( 'alkitab_redirect_from_front_page' ) );

    if ( $redirect_page != home_url( '/' ) ) {
      wp_redirect( $redirect_page );
      die;
    }
  }
}

function alkitab_add_logout_menu_item( $items, $args ) {
  if ( is_user_logged_in() && $args->theme_location == 'profile' ) {
    $items .= "<li><a href='" . wp_logout_url( home_url( '/' ) ) . "'>" . __( 'Log out', 'alkitab' ) . "</a></li>";
  }
  return $items;
}