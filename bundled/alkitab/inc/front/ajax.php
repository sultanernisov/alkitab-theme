<?php

function alkitab_setup_ajax() {
  add_action( 'wp_ajax_alkitab_taxonomy_feed', 'alkitab_taxonomy_feed' );
  add_action( 'wp_ajax_nopriv_alkitab_taxonomy_feed', 'alkitab_taxonomy_feed' );
}

function alkitab_taxonomy_feed() {
  if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
    $tax = $_GET['taxonomy'];
    $term = $_GET['term'];
    $page = $_GET['page'];

    $args = array(
      'post_type' => 'sfwd-courses',
      'paged' => $page,
      'tax_query' => array(
        array(
          'taxonomy' => $tax,
          'field' => 'term_id',
          'terms' => $term
        )
      )
    );

    $query = new WP_Query($args);
    ob_start();

    if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        require get_template_directory() . '/templates/courses-card.php';
      }
    }
    $html = ob_get_contents();
    ob_clean();

    wp_send_json(
      array(
        'pages' => $query->max_num_pages,
        'courses' => $html
      )
    );
    wp_reset_postdata();
  }
}