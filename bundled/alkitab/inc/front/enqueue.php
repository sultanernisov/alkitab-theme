<?php

function alkitab_enqueue_scripts() {
  $localization = array(
    'ajaxurl' => admin_url( 'admin-ajax.php' ),
  );

  wp_register_style( 'main', get_template_directory_uri() . '/assets/css/main.css' );
  wp_enqueue_style( 'main' );

  wp_register_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), false, true );
  wp_localize_script( 'main', 'alkitab', $localization );
  wp_enqueue_script( 'main' );
}
