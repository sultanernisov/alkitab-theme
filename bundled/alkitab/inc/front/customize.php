<?php

function alkitab_customize_register($wp_customize) {

  $wp_customize->add_section( 'alkitab_front_page', array(
    'title' => __( 'Front Page Text', 'alkitab' ),
    'priority' => 200,
    'capability' => 'edit_theme_options'
  ) );

  $wp_customize->add_section( 'alkitab_about_page', array(
    'title' => __( 'About Us Text', 'alkitab' ),
    'priority' => 250,
    'capability' => 'edit_theme_options'
  ) );

  $wp_customize->add_section( 'alkitab_explore_link', array(
    'title' => __( 'Explore Page Link', 'alkitab' ),
    'priority' => 300,
    'capability' => 'edit_theme_options'
  ) );

  $wp_customize->add_section( 'alkitab_redirect_from_front_page', array(
    'title' => __( 'Redirect from front page', 'alkitab' ),
    'priority' => 300,
    'capability' => 'edit_theme_options'
  ) );

  // Front page settings
  $wp_customize->add_setting( 'alkitab_front_page_hero_heading', array(
    'type' => 'theme_mod',
    'default' => __( 'Lorem Ipsum', 'alkitab' ),
  ) );
  
  $wp_customize->add_setting( 'alkitab_front_page_hero_subheading', array(
    'type' => 'theme_mod',
    'default' => __( 'dolor sit amet', 'alkitab' )
  ) );

  $wp_customize->add_setting( 'alkitab_front_page_hero_copy', array(
    'type' => 'theme_mod',
    'default' => __( 'consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'alkitab' )
  ) );

  $wp_customize->add_setting( 'alkitab_front_page_latest_courses_heading', array(
    'type' => 'theme_mod',
    'default' => __( 'Our latest courses', 'alkitab' )
  ) );

  $wp_customize->add_setting( 'alkitab_front_page_cta_heading', array(
    'type' => 'theme_mod',
    'default' => __( 'Lorem Ipsum', 'alkitab' )
  ) );

  $wp_customize->add_setting( 'alkitab_front_page_cta_copy', array(
    'type' => 'theme_mod',
    'default' => __( 'consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'alkitab' )
  ) );

  $wp_customize->add_setting( 'alkitab_front_page_cta_button', array(
    'type' => 'theme_mod',
    'default' => __( 'start now', 'alkitab' )
  ) );

  // Front page controls
  $wp_customize->add_control( 'alkitab_front_page_hero_heading', array(
    'type' => 'text',
    'section' => 'alkitab_front_page',
    'label' => __( 'Hero heading', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_front_page_hero_subheading', array(
    'type' => 'text',
    'section' => 'alkitab_front_page',
    'label' => __( 'Hero subheading', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_front_page_hero_copy', array(
    'type' => 'textarea',
    'section' => 'alkitab_front_page',
    'label' => __( 'Hero copy', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_front_page_latest_courses_heading', array(
    'type' => 'text',
    'section' => 'alkitab_front_page',
    'label' => __( 'Latest courses heading', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_front_page_cta_heading', array(
    'type' => 'text',
    'section' => 'alkitab_front_page',
    'label' => __( 'CTA heading', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_front_page_cta_copy', array(
    'type' => 'textarea',
    'section' => 'alkitab_front_page',
    'label' => __( 'CTA copy', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_front_page_cta_button', array(
    'type' => 'text',
    'section' => 'alkitab_front_page',
    'label' => __( 'CTA button label', 'alkitab' )
  ) );

  // About Page settings
  $wp_customize->add_setting( 'alkitab_about_page_heading', array(
    'type' => 'theme_mod',
    'default' => __( 'Who we are', 'alkitab' )
  ) );

  $wp_customize->add_setting( 'alkitab_about_page_copy', array(
    'type' => 'theme_mod',
    'default' => __( 'Lorem ipsum dolor sit amet', 'alkitab' )
  ) );

  $wp_customize->add_setting( 'alkitab_about_page_section_two_heading', array(
    'type' => 'theme_mod',
    'default' => __( 'How do we work?', 'alkitab' )
  ) );

  $wp_customize->add_setting( 'alkitab_about_page_section_two_subheading', array(
    'type' => 'theme_mod',
    'default' => __( 'Knowledge subheading text', 'alkitab' )
  ) );

  $wp_customize->add_setting( 'alkitab_about_page_section_two_copy', array(
    'type' => 'theme_mod',
    'default' => __( 'Lorem ipsum dolor sit amet', 'alkitab' )
  ) );

  $wp_customize->add_setting( 'alkitab_about_page_section_three_copy', array(
    'type' => 'theme_mod',
    'default' => __( 'Lorem ipsum dolor sit amet', 'alkitab' )
  ) );

  // About Page controls
  $wp_customize->add_control( 'alkitab_about_page_heading', array(
    'type' => 'text',
    'section' => 'alkitab_about_page',
    'label' => __( 'Heading', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_about_page_copy', array(
    'type' => 'textarea',
    'section' => 'alkitab_about_page',
    'label' => __( 'Copy', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_about_page_section_two_heading', array(
    'type' => 'text',
    'section' => 'alkitab_about_page',
    'label' => __( 'Section Two Heading', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_about_page_section_two_subheading', array(
    'type' => 'text',
    'section' => 'alkitab_about_page',
    'label' => __( 'Section Two Subheading', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_about_page_section_two_copy', array(
    'type' => 'textarea',
    'section' => 'alkitab_about_page',
    'label' => __( 'Section Two Copy', 'alkitab' )
  ) );

  $wp_customize->add_control( 'alkitab_about_page_section_three_copy', array(
    'type' => 'textarea',
    'section' => 'alkitab_about_page',
    'label' => __( 'Section Three Copy', 'alkitab' )
  ) );

  // Explore page link settings and controls
  $wp_customize->add_setting( 'alkitab_explore_page_link', array(
    'type' => 'theme_mod',
    'default' => home_url( '/explore' )
  ) );

  $wp_customize->add_control( 'alkitab_explore_page_link', array(
    'type' => 'dropdown-pages',
    'section' => 'alkitab_explore_link',
    'label' => __( 'Select page for explore button', 'alkitab' )
  ) );

  // Redirect from front page
    $wp_customize->add_setting( 'alkitab_redirect_from_front_page', array(
      'type' => 'theme_mod',
      'default' => null
    ) );

    $wp_customize->add_control( 'alkitab_redirect_from_front_page', array(
      'type' => 'dropdown-pages',
      'section' => 'alkitab_redirect_from_front_page',
      'label' => __( 'Select page to redirect logged in users from front page', 'alkitab' )
    ) );
}