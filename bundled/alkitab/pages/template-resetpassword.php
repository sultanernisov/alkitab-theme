<?php
/**
 * Template Name: Reset Password Page
 */

get_header( 'transition' );
?>
<div class="c-resetpassword-page" data-section="transparent">
  <div class="c-resetpassword-container">
    <?php 
      if ( shortcode_exists( 'alkitab-reset-password' ) ) {
        echo do_shortcode( '[alkitab-reset-password]' );
      }
    ?>
  </div>
</div>

<?php

get_footer( 'footerless' );