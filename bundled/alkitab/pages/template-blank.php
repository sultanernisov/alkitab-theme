<?php
/**
 *  Template Name: Blank Page
 */

if ( is_user_logged_in() ) {
  get_header( 'logged' );
} else {
  get_header();
}

if ( have_posts() ) {
  while ( have_posts() ) {
    the_post();
    the_content();
  }
}

get_footer();