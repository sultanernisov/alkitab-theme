<?php
/*
  Template Name: About Page
*/

if ( is_user_logged_in() ) {
  get_header( 'logged-transition' );
} else {
  get_header( 'transition' );
}
?>

  <section class="c-fullscreen-section c-about-first-section" data-section="baby-teal">
      <div class="c-about-first-section--content">
        <h1><?= get_theme_mod( 'alkitab_about_page_heading' ); ?></h1>
        <p><?= get_theme_mod( 'alkitab_about_page_copy' ); ?></p>
      </div>
      <div class="c-about-first-section--tutors">
        <?php
          $tutors = get_users( array( 'role' => 'tutor' ) );

          foreach ( $tutors as $tutor ) { ?>
            <img src="<?= get_avatar_url( $tutor->ID ); ?>" class="c-about-first-section--tutor">
        <?php
          }
        ?>
      </div>
  </section>

  <section class="c-fullscreen-section c-about-second-section" data-section="bright-green-light">
    <div class="c-about-second-section--content">
      <div class="c-about-second-section--text">
        <h2><?= get_theme_mod( 'alkitab_about_page_section_two_heading' ); ?></h2>
        <h6><?= get_theme_mod( 'alkitab_about_page_section_two_subheading' ) ?></h6>
        <p><?= get_theme_mod( 'alkitab_about_page_section_two_copy' ); ?></p>
      </div>
      <img src="<?= get_template_directory_uri() . '/assets/images/quiz.png'; ?>" class="c-about-second-section--image">
    </div>
  </section>

  <section class="c-fullscreen-section c-about-third-section" data-section="teal">
    <div class="c-about-third-section--content">
      <p><?= get_theme_mod( 'alkitab_about_page_section_three_copy' ); ?></p>
      <img src="<?= get_template_directory_uri() . '/assets/images/books.png'; ?>">
    </div>
  </section>

<?php get_footer(); ?>