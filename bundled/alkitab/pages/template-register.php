<?php
/**
 * Template Name: Register Page
 */

get_header( 'transition' );
?>
<div class="c-register-page" data-section="transparent">

  <?php
    if ( shortcode_exists( 'alkitab-register' ) ) {
      echo do_shortcode( '[alkitab-register]' );
    }
  ?>

</div>

<?php
get_footer( 'footerless' );