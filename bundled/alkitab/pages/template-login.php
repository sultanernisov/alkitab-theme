<?php
/**
 * Template Name: Log In Page
 */

get_header( 'transition' ); ?>

<div class="c-login-page" data-section="transparent">
  <div class="c-login-container">
    <?php
      if ( shortcode_exists( 'alkitab-login' ) ) {
        echo do_shortcode( '[alkitab-login]' );
      }
    ?>
  </div>

  <div class="c-login-page-decoration">
      <?php
        if ( get_option( 'alkitab_login_image' ) ):
          $image_url = wp_get_attachment_image_url( get_option( 'alkitab_login_image' ), 'full' );
      ?>
        <?php if ( get_option( 'alkitab_login_image_full' ) ): ?>
          <img class="c-login-page-decoration--image-full" src="<?= $image_url ?>">
        <?php else: ?>
          <img class="c-login-page-decoration--image" src="<?= $image_url ?>">
        <?php endif; ?>
      <?php else: ?>
        <img class="c-login-page-decoration--default" src="<?= get_template_directory_uri() . '/assets/images/video.png'; ?>" >
      <?php endif; ?>
  </div>
</div>

<?php get_footer( 'footerless' );