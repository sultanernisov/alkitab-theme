<form class="c-searchform" action="<?= home_url(); ?>" method="GET">
  <i data-feather="search"></i>
  <input class="c-searchform--input" type="text" name="s" placeholder="<?= __( 'Search for courses', 'alkitab' ); ?>" value="<?php the_search_query(); ?>" autocomplete="off">
</form>