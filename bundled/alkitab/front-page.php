<?php
  if ( is_user_logged_in() ) {
    get_header( 'logged-transition' );
  } else {
    get_header( 'transition' );
  }
?>

  <section class="c-fullscreen-section c-hero" data-section="baby-teal">
    <img src="<?= get_template_directory_uri() . '/assets/images/hero.png'; ?>" class="c-hero--image">
    <div class="c-hero--content">
      <h1 class="c-hero--heading"><?= get_theme_mod( 'alkitab_front_page_hero_heading' ); ?></h1>
      <h2 class="c-hero--subheading"><?= get_theme_mod( 'alkitab_front_page_hero_subheading' ); ?></h2>
      <p class="c-hero--copy"><?= get_theme_mod( 'alkitab_front_page_hero_copy' ); ?></p>
    </div>
  </section>

  <section class="c-fullscreen-section c-latest-courses" data-section="teal">
    <h2 class="c-latest-courses--heading"><?= get_theme_mod( 'alkitab_front_page_latest_courses_heading' ); ?></h2>

    <div class="c-latest-courses--list">

      <?php

        $args = array(
          'post_type' => 'sfwd-courses',
          'posts_per_page' => 6
        );

        $courses = new WP_Query( $args );

        if ( $courses->have_posts() ) {
          while ( $courses->have_posts() ) {
            $courses->the_post();

            get_template_part( 'templates/courses', 'frontcard' );
          }
        }

        wp_reset_postdata();

      ?>


    </div>

  </section>

  <section class="c-fullscreen-section c-cta" data-section="teal">
    <div class="c-cta--content">
      <h2 class="c-cta--heading"><?= get_theme_mod( 'alkitab_front_page_cta_heading' ); ?></h2>
      <p class="c-cta--copy"><?= get_theme_mod( 'alkitab_front_page_cta_copy' ); ?></p>
      <div class="c-cta--button-container">
        <a
          href="<?= get_theme_mod( 'alkitab_front_page_cta_link', home_url('/explore') ); ?>"
          class="c-cta--button"
        >
            <?= get_theme_mod( 'alkitab_front_page_cta_button' ); ?>
        </a>
        <img src="<?= get_template_directory_uri() . '/assets/images/click.png'; ?>" class="c-cta--click">
      </div>
    </div>
  </section>

<?php get_footer(); ?>