<?php
/**
 *  Template Name: Lost Password Page
 */

get_header( 'transition' ); ?>


<div class="c-lostpassword-page" data-section="transparent">
  <div class="c-lostpassword-container">
    <?php

      if ( shortcode_exists( 'alkitab-lost-password' ) ) {
        echo do_shortcode( '[alkitab-lost-password]' );
      }

    ?>
  </div>
</div>


<?php
get_footer( 'footerless' );