<?php
  $logotypes = array(
    'dark' => get_template_directory_uri() . '/assets/images/logo-dark.png',
    'light' => get_template_directory_uri() . '/assets/images/logo-light.png',
    'default' => get_template_directory_uri() . '/assets/images/logo.png',
  );
?>
<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php bloginfo( 'title' ); ?></title>
  <?php wp_head(); ?>
</head>
<body>
  <!-- ROOT -->
  <div id="root">

    <!-- NAVIGATION -->
    <nav id="navigation" class="l-nav-container">
      <!-- MOBILE -->
      <div class="l-mnav" data-mobile-navigation>
        <div class="c-mnav">
          <i class="c-mnav--menu" data-menu-toggle data-feather="menu"></i>
          <a class="c-mnav--homelink" href="<?= home_url(); ?>">
            <img
              class="c-mnav--logo c-logo--dark"
              src="<?php echo $logotypes['dark']; ?>"
              alt="alkitab logo"
            >
            <img
              class="c-mnav--logo c-logo--light"
              src="<?php echo $logotypes['light']; ?>"
              alt="alkitab logo"
            >
            <img
              class="c-mnav--logo c-logo"
              src="<?= $logotypes['default']; ?>"
              alt="alkitab logo"
            >
          </a>
          <i class="c-mnav--search" data-search-toggle data-feather="search"></i>
        </div>

        <div class="c-offcanvas" data-offcanvas>
          <div class="c-mprofile">
            <img src="<?= get_avatar_url( get_current_user_id() ); ?>" class="c-avatar">
            <span class="c-mprofile--username">
              <?php
                echo __( 'Welcome back,', 'alkitab' );
                echo wp_get_current_user()->display_name . "!";
              ?>
            </span>
          </div>
          <a class="c-mprofile--explore" href="<?= get_permalink( get_theme_mod( 'alkitab_explore_page_link' ) ); ?>"><?= get_the_title( get_theme_mod( 'alkitab_explore_page_link' ) ); ?></a>
          <?php
            wp_nav_menu( array(
              'theme_location' => 'profile',
              'container' => false,
              'menu_class' => 'c-offcanvas--menu'
            ) );
          ?>

        </div>

        <div class="c-searchbar">
          <?php get_search_form(); ?>
        </div>

        <div class="c-overlay" data-overlay></div>
      </div><!-- END MOBILE -->

      <!-- TABLET-UP -->
      <div class="c-nav grid">
        <a class="c-nav--homelink grid default" href="<?= home_url(); ?>">
          <img
            class="c-nav--logo c-logo--dark"
            src="<?= $logotypes['dark'] ?>"
            alt="alkitab logo"
          >
          <img
            class="c-nav--logo c-logo--light"
            src="<?= $logotypes['light'] ?>"
            alt="alkitab logo"
          >
          <img
              class="c-nav--logo c-logo"
              src="<?= $logotypes['default']; ?>"
              alt="alkitab logo"
            >
        </a>
        <div class="c-nav--search-container grid">
            <?php get_search_form(); ?>
        </div>
        <a class="c-nav--explore grid" href="<?= get_permalink( get_theme_mod( 'alkitab_explore_page_link' ) ) ?>"><?= get_the_title( get_theme_mod( 'alkitab_explore_page_link' ) ); ?></a>
        <div class="c-profile grid">
            <img src="<?= get_avatar_url( get_current_user_id() ); ?>" class="c-avatar">
            <div class="c-profile-menu--container">
              <?php
                wp_nav_menu( array(
                  'theme_location' => 'profile',
                  'container' => false,
                  'menu_class' => 'c-profile-menu'
                ) );
              ?>
            </div>
        </div>
      </div><!-- END TABLET-UP -->
    </nav><!-- END NAVIGATION -->