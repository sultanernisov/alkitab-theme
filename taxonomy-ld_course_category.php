<?php

if ( is_user_logged_in() ) {
  get_header( 'logged' );
} else {
  get_header();
}

$current_term = get_queried_object();
?>

<div class="c-taxonomy--header">
  <div class="c-taxonomy--header-details">
    <h1><?= esc_attr( $current_term->name ); ?></h1>
    <p><?= $current_term->description; ?></p>
  </div>
</div>

<div class="c-taxonomy--courses">
  <div
    class="c-taxonomy--feed" 
    data-taxonomy="<?= $current_term->taxonomy; ?>"
    data-term="<?= $current_term->term_id; ?>"
  >

  </div>
</div>

<?php
get_footer();